import items from './items';
import services from './services';
export default () => ({
  services,
  items,
});
