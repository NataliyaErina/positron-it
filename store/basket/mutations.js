export default {
  onClear: state => {
    state.items = [];
    state.services = [];
  },
  onDelete: (state, articule) => {
    state.items = state.items.filter(item => item.articule !== articule);
  },
  onChangeCount: (state, obj) => {
    if (obj.articule && obj.count === 0) {
      state.items = state.items.filter(item => item.articule !== obj.articule);
    } else if (obj.articule && state.items.find(item => item.articule === obj.articule)) {
      state.items.find(item => item.articule === obj.articule).count = obj.count;
    }
  },
  onToggleService: (state, obj) => {
    if (obj.value) {
      state.services.push(obj.name);
    } else {
      state.services = state.services.filter(item => item !== obj.name);
    }
  },
};
