export default {
  total: (state, getters, rootState) =>
    state.items.reduce((summ, item) => item.price[rootState.core.lang] * item.count + summ, 0),
  count: state => state.items.reduce((summ, item) => item.count + summ, 0),
  isService: state => state.services.includes('service'),
  formData: state => {
    /* обработка данных, перевод в нужный вид, добавление доп полей и т.п. */
    return state.items;
  },
};
