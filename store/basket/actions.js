export default {
  async onSend(store) {
    try {
      const result = await this.$axios.$post('/', store.getters.formData);
      /* обработка полученного ответа */
      if (result.success) {
        // store.commit('setIsSuccess', true);
      } else {
        // store.commit('setError', result.message);
      }
    } catch (e) {
      console.log('onSend', e);
    }
  },
};
