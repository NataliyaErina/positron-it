require('dotenv').config();

export default {
  htmlAttrs: {
    lang: 'ru',
  },
  head: {
    title: 'positron',
    meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' }],
    __dangerouslyDisableSanitizers: ['script', 'noscript'],
  },
  loading: { color: '#fff' },
  router: {
    base: '/',
  },
  css: ['~/assets/scss/common.scss'],
  plugins: [{ src: '~/plugins/utils.js' }, { src: '~/plugins/vendor.js', ssr: false }],
  styleResources: {
    scss: ['assets/scss/resources.scss'],
  },
  buildModules: ['@nuxtjs/eslint-module'],
  modules: ['@nuxtjs/axios', '@nuxtjs/style-resources', '@nuxtjs/dotenv'],
  axios: {},
};
