const utils = {};

utils.declineNumeral = function(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
};

utils.formatNumber = function(val, thSep, dcSep) {
  if (!val) return 0;
  // Проверка указания разделителя разрядов
  if (!thSep) thSep = ' ';

  // Проверка указания десятичного разделителя
  if (!dcSep) dcSep = ',';

  const res = val.toString();
  const lZero = val < 0; // Признак отрицательного числа

  // Определение длины форматируемой части
  let fLen = res.lastIndexOf('.'); // До десятичной точки
  fLen = fLen > -1 ? fLen : res.length;

  // Выделение временного буфера
  let tmpRes = res.substring(fLen);
  let cnt = -1;
  for (let ind = fLen; ind > 0; ind--) {
    // Формируем временный буфер
    cnt++;
    if (cnt % 3 === 0 && ind !== fLen && (!lZero || ind > 1)) {
      tmpRes = thSep + tmpRes;
    }
    tmpRes = res.charAt(ind - 1) + tmpRes;
  }

  return tmpRes.replace('.', dcSep);
};

utils.formatPrice = function(price, lang = 'ru') {
  return utils.formatNumber(price) + (lang === 'ru' ? ' ₽' : ' €');
};

export default ({ app }, inject) => {
  inject('utils', utils);
};
